list=[]#In this empty list must be contain our objects for Computer class
class Computer:

	#This function is strocting function
	def __init__(self,name,cpu,ram,hdd):
		self.name = name
		self.cpu = cpu
		self.ram = ram
		self.hdd = hdd

	'''
	This function compares the parameters of computers(objects) if any of the parameters has the same value with other the
	function the function prints names of that computers and about likely parameters
	'''		
	def comparing_parameters(self,other):
		if  self.cpu == other.cpu:
			print '%s and %s computers have the same processor frequency' % (self.name,other.name)
		elif self.ram == other.ram:
			print '%s and %s computers have the same Random Acces Meamory' % (self.name,other.name)
		elif self.hdd == other.hdd:
			print '%s and %s computers have the same volume of hard disc meamory' % (self.name,other.name)
		else:
			print '%s and %s have not same parameters' % (self.name,other.name)

#Here we discribe objects with their attributes and them in list 
p1 = Computer("Asus", "1.8 Ghz", "2048 Mb", "500 Gb")
p2 = Computer("Dell", "1.8 Ghz", "1024 Mb", "500 Gb")
p3 = Computer("Acer", "2.4 Ghz", "2048 Mb", "1 Tb")
p4 = Computer("Samsung", "3.0 Ghz", "4096 Mb", "500 Gb")
p5 = Computer("Sony", "3.2 Ghz", "8 Gb", "2 Tb")
list.append(p1)
list.append(p2)
list.append(p3)
list.append(p4)
list.append(p5)

#This cycle drives "comparing_parameters" function for all enumbered objects
for i in range(0,5):
	for j in range(i+1,5):
		list[i].comparing_parameters(list[j])
